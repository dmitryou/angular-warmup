'use strict';

angular
    .module('warmUpApp')
    .controller('NavBarCtrl', [
        '$scope',
        'navBarService',
        NavBarCtrl]);

function NavBarCtrl($scope, navBarService) {

    $scope.weChoisingStartPoint = navBarService.choisingStartPointNow();
    $scope.setChoisingStartPoint = setChoisingStartPoint;
    $scope.setChoisingEndPoint = setChoisingEndPoint;
    $scope.goPressed = goPressed;



    function setChoisingStartPoint() {
        navBarService.setChoisingStartPoint();
        /**
         * Disable other options
         */
        navBarService.resetChoisingEndPoint();
    }

    function setChoisingEndPoint() {
        navBarService.setChoisingEndPoint();
        /**
         * Disable other options
         */
        navBarService.resetChoisingStartPoint();
    }

    function goPressed() {
        $scope.$broadcast('findThePath', { message: 'kuku' });
    }



}
