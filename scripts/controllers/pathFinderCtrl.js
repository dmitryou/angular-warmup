'use strict';

angular
    .module('warmUpApp')
    .controller('PathFinderCtrl', [
        '$scope',
        'optionsService',
        'navBarService',
        'utils',
        PathFinderCtrl]);

function PathFinderCtrl($scope, optionsService, navBarService, utils) {

    $scope.goodColor = optionsService.getOptions().colorA;
    $scope.blockerColor = optionsService.getOptions().colorB;
    $scope.startPointColor = 'yellow';
    $scope.endPointColor = 'brown';
    $scope.fieldSize = 12;
    $scope.lastStartPoint = null;
    $scope.lastEndPoint = null;
    $scope.swapColor = swapColor;
    $scope.findPath = findPath;


    _initBoard();

    function _initBoard(){

        $scope.board = utils.create2DArray($scope.fieldSize,$scope.goodColor);
        console.log('$scope.board', $scope.board);

    }

    $scope.$on('findThePath', function (event, args) {
        console.log(args.message);
        $scope.findPath();
    });

    function swapColor(color,x,y) {
        console.log('PathFinderCtrl', color,x,y,$scope.weChoisingStartPoint);
        if(navBarService.choisingStartPointNow()) {
            if(utils.samePoint({x:x,y:y}, $scope.lastEndPoint)){
                alert('Cannot use same point for Start and End');
                return;
            } else if($scope.lastStartPoint) {
                $scope.board[$scope.lastStartPoint.x][$scope.lastStartPoint.y].color = $scope.goodColor;
            }
            $scope.lastStartPoint = {
                x: x,
                y: y
            };
            $scope.board[x][y].color = $scope.startPointColor;
            //Only Once
            $scope.board[x][y].isStartPoint = true;
            navBarService.resetChoisingStartPoint();
            return;
        } else if(navBarService.choisingEndPointNow()){
            if(utils.samePoint($scope.lastStartPoint, {x:x,y:y})){
                alert('Cannot use same point for Start and End');
                return;
            } else if($scope.lastEndPoint) {
                $scope.board[$scope.lastEndPoint.x][$scope.lastEndPoint.y].color = $scope.goodColor;
            }
            $scope.lastEndPoint = {
                x: x,
                y: y
            };
            $scope.board[x][y].color = $scope.endPointColor;
            //Only Once
            $scope.board[x][y].isEndPoint = true;
            navBarService.resetChoisingEndPoint();
            return;
        }


        if(color === $scope.goodColor) {
            $scope.board[x][y].color = $scope.blockerColor;
            $scope.board[x][y].isStartPoint = false;
            $scope.board[x][y].isEndPoint = false;
        } else {
            $scope.board[x][y].color = $scope.goodColor;
            $scope.board[x][y].isStartPoint = false;
            $scope.board[x][y].isEndPoint = false;
        }
    }

    function findPath() {
        /**
         * Check if have start and End points
         */
        if(utils.haveStartAndEndPoints($scope.board)) {
            alert('Run Awesome ALGO and ANIMATION !');
        } else {
            alert('Please choice Start and End points !');
        }
    }

}
