'use strict';
angular
    .module('warmUpApp')
    .factory('navBarService', [
        navBarService
    ]);

function navBarService() {
    var factory = {
        choisingStartPointNow: choisingStartPointNow,
        setChoisingStartPoint: setChoisingStartPoint,
        resetChoisingStartPoint: resetChoisingStartPoint,
        choisingEndPointNow: choisingEndPointNow,
        setChoisingEndPoint: setChoisingEndPoint,
        resetChoisingEndPoint: resetChoisingEndPoint,
    };
    var weChoisingStartPoint = false;
    var weChoisingEndPoint = false;
    var startPathFind = false;

    return factory;

    /**
     * Setting, resetting choice option
     */
    function choisingStartPointNow() {
        return weChoisingStartPoint;
    }

    function setChoisingStartPoint() {
        weChoisingStartPoint = true;
    }

    function resetChoisingStartPoint() {
        weChoisingStartPoint = false;
    }

    function choisingEndPointNow() {
        return weChoisingEndPoint;
    }

    function setChoisingEndPoint() {
        weChoisingEndPoint = true;
    }

    function resetChoisingEndPoint() {
        weChoisingEndPoint = false;
    }
}

