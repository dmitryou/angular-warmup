'use strict';


angular
    .module('warmUpApp')
    .factory('optionsService', [
        optionsService
    ]);

function optionsService() {
    var factory = {
        getOptions: getOptions,
        setColorA: setColorA,
        setColorB: setColorB,
        setColorC: setColorC,
    };
    var options = {
        colorA:'red',
        colorB:'green',
        colorC:'green',
    }
    return factory;

    function getOptions() {
        return options;
    }

    function setColorA(color) {
        options.colorA = color;
    }

    function setColorB(color) {
        options.colorB = color;
    }

    function setColorC(color) {
        options.colorC = color;
    }
}

