'use strict';


angular
    .module('warmUpApp')
    .factory('utils', [
        utils
    ]);

function utils() {
    var factory = {
        create2DArray: create2DArray,
        samePoint: samePoint,
        haveStartAndEndPoints: haveStartAndEndPoints,
    };
    return factory;

    function create2DArray(size, color) {
        var arr = []; // Initialize array
        for (var i = 0 ; i < size; i++) {
            arr[i] = []; // Initialize inner array
            for (var j = 0; j < size; j++) { // i++ needs to be j++
                arr[i][j] = {
                    x:i,
                    y:j,
                    color: color,
                    isStartPoint: false,
                    isEndPoint: false
                };
            }
        }
        return arr;
    }

    function haveStartAndEndPoints(arr) {
        var haveStart = false;
        var haveEnd = false;
        for (var i = 0 ; i < arr.length; i++) {
            for (var j = 0; j < arr[i].length; j++) {
                if(arr[i][j].isStartPoint === true) {
                    haveStart = true;
                }
                if(arr[i][j].isEndPoint === true) {
                    haveEnd = true;
                }
            }
        }

        console.log('haveStart && haveEnd',haveStart,haveEnd);
        return haveStart && haveEnd;
    }

    function samePoint(point1, point2) {

        if(point1 && point2 && point1.x === point2.x && point1.y === point2.y) {
            return true;
        }
        return false;
    }
}

