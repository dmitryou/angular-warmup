var warmUpApp = angular.module('warmUpApp', ['ui.router']);

warmUpApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/pathFinder');

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
        .state('pathFinder', {
            url: '/pathFinder',
            templateUrl: 'views/pathFinder.html',
            controller: 'PathFinderCtrl',
        })
        .state('settings', {
            url: '/settings',
            templateUrl: 'views/settings.html',
            controller: 'SettingsCtrl',
        })

});
